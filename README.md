#JUEGA AL TIC TAC TOE - LARAVEL POR LUIS LINDARTE.

##Descripción
<p>Aplicación Laravel para jugar Tic Tac Toce en dos pantallas, el usuario que genere la partida deberá compartir el código de acceso para unirse a la partida al segund usuario.</p>

##Instalación
<ol>
<li>Crear base de datos vacia mysql con nombre "laravel_tictactoe" y con colación **utf8mb4_unicode_ci**.</li>
<li>Configurar en el archivo .env que se encuentra en el root del proyecto el nombre de usuaurio y contraseña para acceder a la base de datos, asignar este usuario con todos los permisos a la base de datos.</li>
<li>Ejecutar la migracion con las semillas: **php artisan migrate:fresh --seed**</li>
</ol>

##Cómo usar
<p>En la pantalla del juego, presiona en **Generar partida**, una vez generada aparecerá el código para compartir a un amigo.</p>

<p>Tu amigo debe introducir el código de la partida, para esto debe presionar en **Unirse a partida**. Automáticamente se sincronizará la partida.</p>

<p>Cada jugador debe esperar a que sea su turno para escoger una casilla.</p>

<p>Cuando termine la partida deberá repetirse el proceso de **Generar partida** y compatir el código.</p>

<p>Para cambiar el nombre del usuario presione **Cambiar nombre** y escriba uno nuevo.</p>