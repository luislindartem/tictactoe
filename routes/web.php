<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JuegoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('juego');
});


Route::controller(JuegoController::class)->group(function(){
    //Route::get('/', 'index');
    Route::post('juego/create', 'create');
    Route::get('juego/unirse/{codigop}', 'unirse')->where('codigop',  '[0-9A-Za-z]+');
    Route::get('juego/movimiento/{codigop}/{codigousuario}/{posicion}', 'movimiento')->where('codigop',  '[0-9A-Za-z]+')->where('codigousuario', '[0-9]+')->where('posicion', 'p[1-9]+');
    Route::get('juego/estado/{codigop}/{codigousuario}', 'estado')->where('codigop',  '[0-9A-Za-z]+')->where('codigousuario', '[0-9]+');
    Route::put('juego/cambiarnombrejugador', 'cambiarNombreJugador');
});


