<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJuegoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('juego', function (Blueprint $table) {
            $table->id();
            $table->string('codigop', 16)->unique();            //se genera al crearse, para que no se tenga que usar el id sencuencial.
            $table->string('nombrej1', 16)->nullable();        //debe esperar a que lo escriba
            $table->mediumInteger('codigoj1');                     //se genera al crearse
            $table->string('nombrej2', 16)->nullable();        //debe esperar a que lo escriba
            $table->mediumInteger('codigoj2');                     //se genera al crearse
            $table->tinyInteger('empiezapor');                  //será 1 o 2, empieza a jugarse por jugador 1 o 2.
            $table->tinyInteger('p1')->default(0);
            $table->tinyInteger('p2')->default(0);
            $table->tinyInteger('p3')->default(0);
            $table->tinyInteger('p4')->default(0);
            $table->tinyInteger('p5')->default(0);
            $table->tinyInteger('p6')->default(0);
            $table->tinyInteger('p7')->default(0);
            $table->tinyInteger('p8')->default(0);
            $table->tinyInteger('p9')->default(0);

            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('juego');
    }
}
