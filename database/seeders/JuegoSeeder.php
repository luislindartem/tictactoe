<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Db;

class JuegoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('juego')->insert([
            "codigop" => "AAABBBCCC",
            "nombrej1" => "Luis",
            "codigoj1" => 36366,
            "nombrej2" => "Eduardo",
            "codigoj2" => 88888,
            "empiezapor" => 1,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        ]);

        DB::table('juego')->insert([
            "codigop" => "DDDEEEFFF",
            "nombrej1" => "Maria",
            "codigoj1" => 787856,
            "nombrej2" => "Ana",
            "codigoj2" => 435465,
            "empiezapor" => 1,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s"),
        ]);
    }
}
