        
    let jugando = false;
    let idpartida = ''; 
    let idjugadorpartida = '';  
    let numerojugador = 0;
    let intervalo = false;
    let turnoactual = '';
    var myModal = new bootstrap.Modal(document.getElementById("elmodal"), {});					
    var elmodalnombre = new bootstrap.Modal(document.getElementById("elmodalnombre"), {});					
    
    $(document).ready(function(){                
        

    });	
    /*
        Envía un movimiento al servidor
        @Casilla: La posición que se desea marcar que van desde p1 hasta p9, p1 sería la casillla superior izquierda y p9 sería la casilla inferior derecha, el recorrido se hace recorriendo las filas de arriba hacia abajo.
        Retorna: No hay retorno.
    */
    function movimiento(casilla){
        $.ajax({type: "GET", url: "juego/movimiento/"+idpartida+"/"+idjugadorpartida+'/'+casilla, data:'', success: function(resp){                                   
            switch(resp.estado){
                case 'ok':

                break;
                case 'error':
                    switch(resp.codigo){
                        case 'juego-terminado':
                            alert("El juego ya terminó");
                        break;
                        case 'jugador-invalido':
                            alert("El jugador es inválido"); 
                        break;
                        case 'no-es-su-turno':
                            alert("No es tu turno");
                        break;
                        case 'casilla-ocupada':
                            alert("La casilla ya esta ocupada");   
                        break;
                        case 'juego-no-encontrado':
                            alert("Juego no econtrado");
                        break;                                
                    }
                break;
            }                    
        }});
    }
    /*
        Actualiza la inferfáz gráfica con los datos del juego actuales
        Retorna: no hay retorno.
    */
    function estadoJuego(){
        $.ajax({type: "GET", url: "juego/estado/"+idpartida+"/"+idjugadorpartida, data:'', success: function(resp){                                    
            switch(resp.estado){
                case 'ok':
                    let turno = resp.turno;
                    let matriz = resp.matriz;
                    let cont = 1;                            
                    $('#contrincantes').html(resp.nombrej1+" vs "+resp.nombrej2);

                    $.each(matriz, function(index, ma){
                        $.each(ma, function(index, m){
                            switch(m){
                                case 0:
                                    $('#p'+cont).html('');
                                break;
                                case 1:
                                    $('#p'+cont).html('X');
                                break;
                                case 2:
                                    $('#p'+cont).html('O');
                                break;
                            }                                    
                            cont++;                                
                        });   
                    }); 
                    
                    //escribir de quien es el turno o si ya se termino la partida
                    if(turno.indexOf('gana')!==-1){     //gano alguien
                        if(turno=='gana'+numerojugador){        //gano este jugador
                            alert("Ganaste!");
                        }else{
                            if(turno=='gana0'){ //empatado
                                alert("Es un empate!");
                            }else{      //gano el otro jugador
                                alert("Perdiste!");
                            }
                        }
                        if(intervalo){
                            clearInterval(intervalo);
                            intervalo = false;
                        }
                        turnoactual = '';
                        $('#codigocompartir').html("");
                        $('#botoncambiarnombre').attr('disabled', 'disabled');
                        $('#miturno').hide();
                        $('#otroturno').hide();
                        jugando = false;
                    }else{
                        turnoactual = turno;
                        if(turno=='turno'+numerojugador){   //turno de este jugador
                            $('#otroturno').hide();
                            $('#miturno').show();                                    
                        }else{      //esperando que el otro jugador haga su turno                                    
                            $('#miturno').hide();
                            $('#otroturno').show();
                        }
                    }                            
                break;
                case 'error':
                    
                break;
            }
        }});
    }
    /*
        Establece el interval para poder consumir los datos del estado actual del juego cada segundo.
        retorna: no hay retorno.
    */
    function setLoop(){
        if(!intervalo){
            estadoJuego();
            intervalo = setInterval(function () {
                estadoJuego();
                }, 
            1000);
        }
    }
    /*
        Añade los eventos a los elementos de la interfaz, esta funcion ha de ejecutarse una vez haya cargado la página.
        retorna: No hay retorno.
    */
    function postLoad(){
        $('#botoncambiarnombre').on('click', function(){      
            elmodalnombre.show();
        });
        $('#nombreboton').on('click', function(){                   
            let nuevonombre = $('#inputnombre').val();
            $(this).attr('disabled', 'disabled');
            var data = {                        
                "codigopartida": idpartida,
                "codigousuario": idjugadorpartida,
                "nuevonombre": nuevonombre
            };                        
            $.ajax({type: "PUT", headers: {'X-CSRF-TOKEN': token }, url: "juego/cambiarnombrejugador", data:data, success: function(resp){
                $('#nombreboton').attr('disabled', false);                        
                switch(resp.estado){
                    case 'ok':
                        $('#inputnombre').val('');
                        alert("Nombre cambiado!");
                    break;
                    case 'error':
                        alert("Error al cambiar el nombre");
                    break;
                }
                elmodalnombre.hide();
            }});
        });  
        
        $('#botonnuevapartida').on('click', function(){
            $(this).attr('disabled', 'disabled');
            $.ajax({type: "POST", headers: {'X-CSRF-TOKEN': token }, url: "juego/create", data:'', success: function(resp){                        
                $('#botonnuevapartida').attr('disabled', false);                        
                //resp = jQuery.parseJSON(resp);		
                idpartida = resp.codigop;
                idjugadorpartida = resp.codigoj1;
                jugando = true;
                numerojugador = 1;
                $('#codigocompartir').html(": Comparte este id de partida: "+idpartida);
                $('#botoncambiarnombre').attr('disabled', false);
                alert("Creada!");
                if(intervalo){
                    clearInterval(intervalo);
                    intervalo = false;
                }
                setLoop();  
            }});
        });
        
        $('#botonunirseapartida').on('click', function(){      
            myModal.show();
        });
        $('#unirseboton').on('click', function(){
            let valor = $('#inputunirse').val();                                        
            if(valor!=''){
                $(this).attr('disabled', 'disabled');
                $.ajax({type: "GET", url: "juego/unirse/"+valor, data:'', success: function(resp){                        
                    $('#unirseboton').attr('disabled', false);
                    //resp = jQuery.parseJSON(resp);
                    switch(resp.estado){
                        case 'ok':
                            idpartida = resp.codigop; 
                            idjugadorpartida = resp.codigoj2; 
                            numerojugador = 2;
                            jugando = true;
                            $('#botoncambiarnombre').attr('disabled', false);
                            if(intervalo){
                                clearInterval(intervalo);
                                intervalo = false;
                            }
                            $('#inputunirse').val('');
                            myModal.hide();
                            setLoop();                                   
                        break;
                        case 'error':
                            myModal.hide();
                            switch(resp.codigo){
                                case 'juego-no-encontrado':
                                    alert("Juego no encontrado");
                                break;
                            }
                        break;
                    }
                }});
            }                    
        });

        $("div[class^='space']").each(function(i){
            $(this).on('click', function(){
                if(turnoactual=='turno'+numerojugador){                            
                    movimiento($(this).attr('id'));
                }else{
                    if(jugando){
                        alert("No es tu turno");
                    }
                }
            });
        });     
    } 