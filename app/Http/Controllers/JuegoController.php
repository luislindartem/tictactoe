<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Juego;

class JuegoController extends Controller
{

    public function index(){

        echo 'Retornando interfaz grafica';

    }
    /*
        Genera un juego
        Retorna JSON los datos del luego en formto:
            codigop: el cual es el código de la partida actual,
            nombrej1: El nombre que tiene el juador 1 en la partida
            codigoj1: El código de acceso que tiene el juador 1 en la partida.
            nombrej2: El nombre que tiene el juador 2 en la partida
            codigoj2: El código de acceso que tiene el juador 2 en la partida.
            empiezapor: 1  el numero del jugador que debe empezar jugando en la partida.
     */
    public function create(){
       
        $codigopartida = '';                
        $existe = 1;
        while($existe == 1){
            $codigopartida = Juego::generateCode(9);
            $existe = count(Juego::where('codigop', $codigopartida)->get());
        }
        
        Juego::create([
            'codigop' => $codigopartida,
            'nombrej1' => 'Jugador 1',
            'codigoj1' => rand(1, 999999),
            'nombrej2' => 'Jugador 2',
            'codigoj2' => rand(1, 999999),
            'empiezapor' => 1,
        ]);
        
        $ultimo = Juego::latest()->first();

        return response()->json([
            'codigop' => $ultimo->codigop,
            'codigoj1' => $ultimo->codigoj1
        ]);
    }

    /*  
        Se une a una partida.
        @$codigopartida : String, es el código de la partida a la que se desea unir.
        Retorna json:
            estado:    'ok' o 'error'  si se unió correctamente o si hubo algún error respectivamente
                si estado es 'ok':
                    codigop: el código de la partida.
                    codigoj2: el código del jugador 2 en esta partida para que sea usado para enviar movimientos
                si estado es 'error':
                    codigo: 'juego-no-encontrado': La partida no fue localizada.
    */
    public function unirse($codigopartida){        
        
        $codigopartida = mb_strtoupper($codigopartida, 'UTF-8');             
        $partida = Juego::where('codigop', $codigopartida)->get();
        if(count($partida)==1){
            foreach($partida as $p){
                return response()->json([
                    'estado' => 'ok',
                    'codigop' => $codigopartida,
                    'codigoj2' => $p->codigoj2
                ]);
            }            
        }else{
            return response()->json([
                'estado' => 'error',
                'codigo' => 'juego-no-encontrado'
            ]);
        }
        
    }

    /*
        Realiza un movimiento en una partida
        @$codigopartida: El código de la partida en donde está jugando el usuario.
        @$codigousuario: El código del usuario dentro de la partida que hace el movimiento
        @$posicion: Número de la casilla a marcar del 1 al 9.
        Retorna JSON:
            estado: 'ok'  cuando se realizó el movimiento
            estado: 'error'. Cuando no se pudo realizar el mvimiento.
                'codigo': Un coídigo de error descriptivo con el error que haya sucecdido 
    */
    public function movimiento($codigopartida, $codigousuario, $posicion){
        $partida = Juego::where('codigop', $codigopartida)->get();
        if(count($partida)==1){
            foreach($partida as $p){
                $turno = Juego::getTurno($p);
                if(strpos($turno, 'turno')!==false){
                    if($p->codigoj2 == $codigousuario || $p->codigoj1 == $codigousuario){  
                        $jugador = 1;
                        if($p->codigoj2 == $codigousuario){
                            $jugador = 2;
                        }
                        if(Juego::getTurno($p)=='turno'.$jugador){
                            if($p->$posicion==0){
                                $p->$posicion = $jugador;
                                $p->save();
                                return response()->json([
                                    'estado' => 'ok'
                                ]);
                            }else{
                                return response()->json([
                                    'estado' => 'error',
                                    'codigo' => 'casilla-ocupada'
                                ]);
                            }                                                        
                        }else{
                            return response()->json([
                                'estado' => 'error',
                                'codigo' => 'no-es-su-turno'
                            ]);
                        }                       
                    }else{
                        return response()->json([
                            'estado' => 'error',
                            'codigo' => 'jugador-invalido'
                        ]);
                    }
                }else{
                    return response()->json([
                        'estado' => 'error',
                        'codigo' => 'juego-terminado'
                    ]);
                }
            }  
        }else{
            return response()->json([
                'estado' => 'error',
                'codigo' => 'juego-no-encontrado'
            ]);
        }
    }

    /*
        Retorna el estado de una partida, 
        @$codigopartida: El código de la partida en donde está jugando el usuario.
        @$codigousuario: El código de cualquiera de los dos usuarios dentro de la partida.        
        Retorna JSON:
            estado: 'ok'  cuando retorna el estado correctamente
                'turno': el código del turno actual
                'matriz': los datos de la matriz de 3 x 3
                'nombrej1': El nombre del jugador1 actual.
                'nombrej2': El nombre del jugador2 actual
            estado: 'error' Cuando no se pudo obtener el estado
                'codigo': Un coídigo de error descriptivo con el error que haya sucecdido 
    */
    public function estado($codigopartida, $codigousuario){
        $partida = Juego::where('codigop', $codigopartida)->get();
        if(count($partida)==1){
            foreach($partida as $p){
                if($p->codigoj2 == $codigousuario || $p->codigoj1 == $codigousuario){                    
                    return response()->json([
                        'estado' => 'ok',
                        'turno' => Juego::getTurno($p),
                        'matriz' => Juego::getMatriz($p),
                        'nombrej1' => $p->nombrej1,
                        'nombrej2' => $p->nombrej2
                    ]);
                }else{
                    return response()->json([
                        'estado' => 'error',
                        'codigo' => 'jugador-invalido'
                    ]);
                }                
            }  
        }else{
            return response()->json([
                'estado' => 'error',
                'codigo' => 'juego-no-encontrado'
            ]);
        }
    }

    /*
        Cambia el nombre de un jugador de la partida
        @$request  Objeto con los datos que llegan por PUT.
        Retorna JSON:
            estado: 'ok': Cuando se cambió el nombre satisfactoriamente.
            estado: 'error': Cuando sucedió un algún error intentando cambiar el nombre
                'codigo': Un codigo corto descrptivo del error que sucedió.
    */    
    public function cambiarNombreJugador(Request $request){
        $partida = Juego::where('codigop', $request->codigopartida)->get();
        if(count($partida)==1){
            foreach($partida as $p){
                $turno = Juego::getTurno($p);
                if(strpos($turno, 'turno')!==false){
                    if($p->codigoj2 == $request->codigousuario || $p->codigoj1 == $request->codigousuario){  
                        $jugador = 1;
                        if($p->codigoj2 == $request->codigousuario){
                            $jugador = 2;
                        }
                        $p->{'nombrej'.$jugador} = $request->nuevonombre;
                        $p->save();
                        return response()->json([
                            'estado' => 'ok'
                        ]);
                    }else{
                        return response()->json([
                            'estado' => 'error',
                            'codigo' => 'jugador-invalido'
                        ]);
                    }
                }else{
                    return response()->json([
                        'estado' => 'error',
                        'codigo' => 'juego-terminado'
                    ]);
                }
            }  
        }else{
            return response()->json([
                'estado' => 'error',
                'codigo' => 'juego-no-encontrado'
            ]);
        }
                        

    }

}
