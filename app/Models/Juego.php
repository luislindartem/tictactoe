<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Juego extends Model
{
    use HasFactory;

    protected $table = 'juego';
    protected $fillable = ['codigop', 'nombrej1', 'codigoj1', 'nombrej2', 'codigoj2', 'empiezapor', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9'];

    
    /*
        Genera un codigo alfanumérico al azar
        @$length : EL tamaño del código que se quiere obtener.
        Retorna: String: El codigo alfanumérico generado.
    */ 
    public static function generateCode($length = 16)
    {
            $pool = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';  //abcdefghijklmnopqrstuvwxyz
            return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    /*
        Obtiene la matríz de movimientos actuales del juego
        @$juego: El objeto juego desde el cual se obtienen los datos.
        Retorna Array con los datos de la matriz de 3x3
    */
    public static function getMatriz(Juego $juego){
        return [
            [$juego->p1, $juego->p2, $juego->p3],
            [$juego->p4, $juego->p5, $juego->p6],
            [$juego->p7, $juego->p8, $juego->p9],
        ];
    }

    /*
        Obtiene el turno actual
        Retorna  String:
            'gana0' si ya finalizó el juego y nadie ganó,         
            'gana1': si gana el jugador 1
            'gana2': si gana el jugador 2
            'turno1', si es el turno del jugador 1
            'turno2', si es el turno del jugador 2
    */
    public static function getTurno(Juego $juego){
        //$matriz = $this->getMatriz($juego);
        $matriz = [
            [$juego->p1, $juego->p2, $juego->p3],
            [$juego->p4, $juego->p5, $juego->p6],
            [$juego->p7, $juego->p8, $juego->p9],

        ];
        $siguienteturno = 1;
        $falta = false;
        $gana = 0;       
        $contadores = [0, 0, 0];
        for($i=0; $i<=2; $i++){
            for($j=0; $j<=2; $j++){
                if($matriz[$i][$j]!=0){
                    $contadores[$matriz[$i][$j]]++;
                }else{
                    $falta = true;
                }
            }            
            if($matriz[$i][0]==$matriz[$i][1] && $matriz[$i][1]==$matriz[$i][2]){
                $gana = $matriz[$i][0];
            }

            if($matriz[0][$i]==$matriz[1][$i] && $matriz[1][$i]==$matriz[2][$i]){
                $gana = $matriz[0][$i];
            } 

            if( ($matriz[0][0]==$matriz[1][1] && $matriz[1][1]==$matriz[2][2]) || ($matriz[2][0]==$matriz[1][1] && $matriz[1][1]==$matriz[0][2]) ){
                $gana = $matriz[1][1];
            } 

        }
       
        if($falta && $gana==0){            
            if($contadores[1]>$contadores[2]){
                $siguienteturno = 2;
            }
            return 'turno'.$siguienteturno;
        }else{
            return 'gana'.$gana;            
        }
               
    }

}
